package elevators;
/**
 *
 * @author Jose
 */
public class ElevatorA extends Elevator
{
    private final static char elevatorType = 'A';
    
    //this elevator can go to all floors except the penthouse (10)
    public ElevatorA() 
    {
        super();
    }
    
    //move elevator
    @Override
    public void move(int floor)
    {
        if (floor < -1 || floor > 10) invalidFloorSelectedMessage();
        else 
        {
            if(getEmergencyButtonPushed())
            {
                emergencyButtonPushedMessage();
            }
            else 
            {
                if (floor == 10) unallowedFloorSelectedMessage(floor);
                else
                    if (getFloor() == floor) sameFloorSelectedMessage(floor);
                    else
                        if(getFloor() < floor) up(floor);
                        else down(floor); 
            }
        }
    }
    
    //method to go up
    private void up(int floor)
    {
        //if elevator is at floor 9 it won't be able to go up
        if(getFloor() >= -1 && getFloor() <= 8 )
        {
            changeFloor(floor);
        }
    }
    
    //method to go down
    private void down(int floor)
    {
        if(getFloor() >= 0 && getFloor() <= 9 )
        {
            changeFloor(floor);
        }
    }

    @Override
    public char getElevatorType() 
    {
        return elevatorType;
    }
    
    
}
