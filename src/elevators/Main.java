package elevators;

import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author Jose
 */
public class Main {

    public static void main(String[] args) 
    { 
        
        mainMenu();
    }
    
    //From this menu you'll be able to either call an elevator or just run a simulation
    public static void mainMenu()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println(" Welcome to the main menu! \n Please select an option. Options are: \n C to call an elevator | S to run a simulation");
        char choice = scanner.next().charAt(0);
        switch (choice)
        {
            case 'C' -> callElevator();
            case 'S' -> simulation();
            default -> 
            {
                System.out.println("You've selected an invalid option. Please select a valid option.");
                mainMenu();
            }
        }
    }
    
    //method to call en elevator
    public static void callElevator()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println(" Welcome! \n Please select an elevator. Options are: \n A | B");
        char selectElevator = scanner.next().charAt(0);
        Elevator elevator = selectElevator(selectElevator);
        System.out.println(" You've selected elevator: " + elevator.getElevatorType() + ".");
        scanner.nextLine();
        System.out.println(" Please select a floor. These are the options available: ");
        Elevator.printListOfFloorNames();
        System.out.println(" If you need to press the emergency button type in E. Or type in F if you want to exit the program ");
        String selectFloor = scanner.next();
        moveElevator(elevator,selectFloor);
    }
    
    //this method returns an Elevator object based on the input provided
    public static Elevator selectElevator(char elevator)
    {
        return switch (elevator)
        {
            case 'A' -> new ElevatorA();
            case 'B' -> new ElevatorB();
            default -> throw new IllegalArgumentException("Invalid argument.");
        };
    }
    
    /*
    Method to move the elevator.
    If F is selected the whole program will finish.
    */
    public static void moveElevator(Elevator elevator, String selection)
    {
        if(selection.equals("F")) finish();
        else if(selection.equals("E")) 
        {
            elevator.emergencyButton();
            System.out.println(" Do you wish to press the reset button? Press Y for yes or N for no.");
            Scanner scanner = new Scanner(System.in);
            String sel = scanner.next();
            if(sel.equals("Y"))
            {
                elevator.resetButton();
                continueOnElevator(elevator);
            }
            else
            {
                System.out.println(" The program will finish.");
                finish();
            }
        }
        else
        {
            elevator.move(Integer.parseInt(selection));
            continueOnElevator(elevator);
        }
    }
    
    //method to continue on the elevator
    public static void continueOnElevator(Elevator elevator)
    {
        System.out.println(" Do you wish to go to another floor? \n Type in the number of the floor you want to go, E for the emergency button \n or F to exit the program.");
        Scanner scanner = new Scanner(System.in);
        String sel = scanner.next();
        moveElevator(elevator,sel);
    }
    
    //method to exit the program
    public static void finish()
    {
        System.out.println(" Bye!");
        System.exit(0);
    }
    
    /*
    Method that simulates 100 passengers requesting elevators at random times, from random floors,
    and then requesting to go to random floors once they're inside the elevators.
    */
    public static void simulation()
    {
        Elevator a = new ElevatorA();
        Elevator b = new ElevatorB();
        for(int i = 1; i <= 100; i++)
        {
            System.out.println("This is execution number: " + i);
            a.move(new Random().ints(-1,11).findFirst().getAsInt());
            b.move(new Random().ints(-1,11).findFirst().getAsInt());
        }
    }
}
