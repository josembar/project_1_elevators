package elevators;
/**
 *
 * @author Jose
 */
public class ElevatorB extends Elevator
{
    private final static char elevatorType = 'B';
    
    //this elevator can go to all floors except the basement (-1)
    public ElevatorB() 
    {
        super();
    }
    
    //move elevator
    @Override
    public void move(int floor)
    {
        if (floor < -1 || floor > 10) invalidFloorSelectedMessage();
        else
        {
            if(getEmergencyButtonPushed())
            {
                emergencyButtonPushedMessage();
            }
            else
            {
                if (floor == -1) unallowedFloorSelectedMessage(floor);
                else
                    if (getFloor() == floor) sameFloorSelectedMessage(floor);
                    else
                        if(getFloor() < floor) up(floor);
                        else down(floor);
            }
        }
    }
    
    //method to go down
    private void up(int floor)
    {
        if(getFloor() >= 0 && getFloor() <= 9)
        {
            changeFloor(floor);
        }         
    }
    
    //method to go down
    private void down(int floor)
    {
        if(getFloor() >= 1 && getFloor() <= 10 )
        {
            changeFloor(floor);
        }
    }

    @Override
    public char getElevatorType() 
    {
        return elevatorType;
    }
    
    
}
