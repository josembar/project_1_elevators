package elevators;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jose
 */
public class Elevator {
    
    private int floor;
    private boolean doorsClosed;
    private boolean emergencyButtonPushed;

    public Elevator() 
    {
        this.floor = 0;
        this.doorsClosed = true;
        this.emergencyButtonPushed = false;
    }

    public int getFloor() 
    {
        return floor;
    }

    public void setFloor(int floor) 
    {
        this.floor = floor;
    }

    public boolean getDoorsClosed() 
    {
        return doorsClosed;
    }

    public void setDoorsClosed(boolean doorsClosed) 
    {
        this.doorsClosed = doorsClosed;
    }

    public boolean getEmergencyButtonPushed() 
    {
        return emergencyButtonPushed;
    }

    public void setEmergencyButtonPushed(boolean emergencyButtonPushed) 
    {
        this.emergencyButtonPushed = emergencyButtonPushed;
    }

    
    //the following method prints the movement of the elevator and changes the floor where is at
    public void changeFloor(int floor)
    {
        System.out.println("Elevator " + getElevatorType()+ ". Current floor: " + listOfFloorNames().get(getFloor()) + ". Moving to: " + listOfFloorNames().get(floor) + ".");
        operateDoors();
        if(!getDoorsClosed()) operateDoors();
        setFloor(floor);
        System.out.println("Elevator " + getElevatorType()+ " arrived at: " + listOfFloorNames().get(getFloor()) + ".");
        operateDoors();
    }
    
    //the following methods will open/close the elevator's doors
    public void operateDoors()
    {
        if(getDoorsClosed())
        {
            System.out.println("Opening doors...");
            setDoorsClosed(false);
        }
        
        else
        {
            System.out.println("Closing doors...");
            setDoorsClosed(true);
        } 
    }
    
    //this method add an emergency button functionality to take the elevator to its nearest floor.
    public void emergencyButton()
    {
        emergencyButtonPushedMessage();
        if(getFloor() < 1)
        {
            System.out.println("You'll remain at " + listOfFloorNames().get(getFloor()) + ".");
            operateDoors();
        }
        else
        {
            System.out.println("You'll be taken to the " + listOfFloorNames().get(getFloor() - 1) + ".");
            changeFloor(getFloor() - 1);
        }
        setEmergencyButtonPushed(true);
    }
    
    //this method add a reset buttton functionality.
    public void resetButton()
    {
        if(getEmergencyButtonPushed())
        {
            setEmergencyButtonPushed(false);
            resetButtonPressedMessage();
        }
        else
            resetButtonNotNeededMessage();
    }
    
    //this method returns an unmutable map which contains the floor names.
    public static Map listOfFloorNames()
    {
        Map listOfFloorNames = new HashMap<Integer,String>();
        listOfFloorNames.put(-1, "basement");
        listOfFloorNames.put(0, "lobby");
        listOfFloorNames.put(1, "first floor");
        listOfFloorNames.put(2, "second floor");
        listOfFloorNames.put(3, "third floor");
        listOfFloorNames.put(4, "fourth floor");
        listOfFloorNames.put(5, "fifth floor");
        listOfFloorNames.put(6, "sixth floor");
        listOfFloorNames.put(7, "seventh floor");
        listOfFloorNames.put(8, "eighth floor");
        listOfFloorNames.put(9, "nineth floor");
        listOfFloorNames.put(10, "penthouse");
        
        return Collections.unmodifiableMap(listOfFloorNames);
    }
    
    //this method will print the values stored in listOfFloorNames()
    public static void printListOfFloorNames() 
    {
        listOfFloorNames().forEach(
        (key,value) -> {
                if(key.equals(4)) System.out.println(" " + key + " - " + value + "|");
                else System.out.print(" " + key + " - " + value + "|");
        });
        System.out.println();
    }
    
    //method to print a message when an invalid floor is selected
    public void invalidFloorSelectedMessage()
    {
        System.out.println("Elevator " + getElevatorType()+ ". Invalid option. Please select a valid floor.");
    }
    
    //method to print a message when an unallowed floor is selected
    public void unallowedFloorSelectedMessage(int floor)
    {
        System.out.println("Elevator " + getElevatorType() + ". You cannot go to the " + listOfFloorNames().get(floor) + ". Please select another floor.");
    }
    
    //method to print a message when the same floor is selected
    public void sameFloorSelectedMessage(int floor)
    {
        System.out.println("Elevator " + getElevatorType() + ". You're already at the " + listOfFloorNames().get(floor) + ".");
    }
    
    //method to print a message when the emergency button has been previously pressed
    public void emergencyButtonPushedMessage()
    {
        System.out.println("Elevator " + getElevatorType() + ". The emergency button was pushed. \nYou need to press the reset button to use the elevator again.");
    }
    
    //method to print a message when the reset button is pressed
    public void resetButtonPressedMessage()
    {
        System.out.println("Elevator " + getElevatorType() + ". The reset button's been pressed. \nYou can use the elevator again.");
    }
    
    //method to print a message when the reset button is pressed but it's not needed
    public void resetButtonNotNeededMessage()
    {
        System.out.println("Elevator " + getElevatorType() + ". No need to press the reset button.");
    }
    
    //override methods
    public void move(int floor) {}

    public char getElevatorType() 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
